// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi,
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';

class Vehicle {
  constructor(transportationtype, countryproduction) {
    this.transportationtype = transportationtype;
    this.countryproduction = countryproduction;
  }

  info() {
    console.log(
      `Jenis Kendaraan roda ${this.transportationtype} dari negara ${this.countryproduction}`
    );
  }
}

// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Paja
// 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
// 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
// print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'

class car extends Vehicle {
  constructor(transportationtype, countryproduction, merk, price, percenttax) {
    super(transportationtype, countryproduction);
    this.merk = merk;
    this.price = price;
    this.percenttax = percenttax;
  }

  totalprice() {
    let total = this.price + this.price * (this.percenttax / 100);
    return total;
  }

  info() {
    super.info();
    console.log(
      `Kendaraan ini nama mereknya ${
        this.merk
      } dengan total harga Rp. ${this.totalprice()}`
    );
  }
}

// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil))

// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()
// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

const Vehicle1 = new Vehicle(2, "jepang");
Vehicle1.info();

const mobil = new car(4, "Jerman", "Mercedes", 800000000, 10);
mobil.info();

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/
